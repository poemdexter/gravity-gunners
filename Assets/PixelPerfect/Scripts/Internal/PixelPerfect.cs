﻿using UnityEngine;
using System.Collections;


public static class PixelPerfect {
	public static PixelPerfectCamera mainCamera {get {if (mainCamera_==null) {mainCamera_=MonoBehaviour.FindObjectOfType<PixelPerfectCamera>();} return mainCamera_;}}
	public static PixelPerfectCamera mainCamera_;
	
	public static Vector3 FitToGrid(Vector3 input, float gridSize) {
        if (gridSize <= 0) { Debug.LogError("PixelPerfect.FitToGrid: gridSize is <= 0"); gridSize = 1; }
		return new Vector3((Mathf.Round(input.x/gridSize)*gridSize), (Mathf.Round(input.y/gridSize)*gridSize), input.z);
	}
}

public enum PixelPerfectFitType {Retro, Smooth}

public enum PixelPerfectZoomMode {ConstantZoom, TargetHeight}