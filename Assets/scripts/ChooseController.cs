﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[Serializable]
public struct PlayerDetail
{
    public String name;
    public Sprite nameSprite;
    public Sprite face;
    public Sprite faceBlink;
}

public class PlayerInfo
{
    public Sprite face;
    public Sprite faceBlink;
    public String name;
    public int detailChoice;
    public Sprite suit;
    public int suitChoice;
    public bool isJoined;
    public bool isFinished;

    public PlayerInfo(PlayerDetail detail, Sprite suit)
    {
        this.face = detail.face;
        this.faceBlink = detail.faceBlink;
        this.name = detail.name;
        this.suit = suit;
        detailChoice = 0;
        suitChoice = 0;
        isJoined = false;
        isFinished = false;
    }
}

public class ChooseController : MonoBehaviour
{
    // player selection data objects
    private PlayerInfo p1Info, p2Info, p3Info, p4Info;
    public PlayerDetail[] playerDetails;
    public Sprite[] p1Suits, p2Suits, p3Suits, p4Suits;

    // secret characters
    public PlayerDetail yvDetail, meanieDetail;
    public Sprite yvSuit, meanieSuit;

    // player selection display objects
    public SpriteRenderer p1Name, p2Name, p3Name, p4Name;
    public GameObject p1Arts, p2Arts, p3Arts, p4Arts;
    public SpriteRenderer p1Suit, p2Suit, p3Suit, p4Suit;
    public SpriteRenderer p1Face, p2Face, p3Face, p4Face;

    // 'press a to join' gameobjects
    public GameObject p1JoinA, p2JoinA, p3JoinA, p4JoinA;

    // player spawn gameobjects
    public GameObject playerPrefab;

    private GlobalController globalController;
    private InputManager inputController;
    
    // countdown timer
    public TenSecTimer timer;
    private bool timerStarted;

    private void Start()
    {
        globalController = GameObject.FindGameObjectWithTag("GlobalController").GetComponent<GlobalController>();
        inputController = GameObject.FindGameObjectWithTag("GlobalController").GetComponent<InputManager>();
        p1Info = new PlayerInfo(playerDetails[0], p1Suits[0]);
        p2Info = new PlayerInfo(playerDetails[0], p2Suits[0]);
        p3Info = new PlayerInfo(playerDetails[0], p3Suits[0]);
        p4Info = new PlayerInfo(playerDetails[0], p4Suits[0]);
    }

    private void Update()
    {
        // handle game start countdown
        if (StartTimer())
        {
            if (!timerStarted)
            {
                timerStarted = true;
                timer.StartTimer();
            }
            else if (timer.IsReady())
            {
                globalController.Setup();
                Application.LoadLevel("level1");
            }
        }

        // changed player suit
        if (p1Info.isJoined && !p1Info.isFinished && (inputController.GetUnreadUp(1) || inputController.GetUnreadDown(1)))
        {
            if (inputController.p1YV) { ChangeToVY(p1Info, p1Suit, p1Face, p1Name); }
            else if (inputController.p1Meanie) { ChangeToMeanie(p1Info, p1Suit, p1Face, p1Name); }
            else { ChangeSuit(p1Info, p1Suit, p1Suits, p1Face, p1Name, inputController.p1Up); }
        }
        if (p2Info.isJoined && !p2Info.isFinished && (inputController.GetUnreadUp(2) || inputController.GetUnreadDown(2)))
        {
            if (inputController.p2YV) { ChangeToVY(p2Info, p2Suit, p2Face, p2Name); }
            else if (inputController.p2Meanie) { ChangeToMeanie(p2Info, p2Suit, p2Face, p2Name); }
            else { ChangeSuit(p2Info, p2Suit, p2Suits, p2Face, p2Name, inputController.p2Up); }
        }
        if (p3Info.isJoined && !p3Info.isFinished && (inputController.GetUnreadUp(3) || inputController.GetUnreadDown(3)))
        {
            if (inputController.p3YV) { ChangeToVY(p3Info, p3Suit, p3Face, p3Name); }
            else if (inputController.p3Meanie) { ChangeToMeanie(p3Info, p3Suit, p3Face, p3Name); }
            else { ChangeSuit(p3Info, p3Suit, p3Suits, p3Face, p3Name, inputController.p3Up); }
        }
        if (p4Info.isJoined && !p4Info.isFinished && (inputController.GetUnreadUp(4) || inputController.GetUnreadDown(4)))
        {
            if (inputController.p4YV) { ChangeToVY(p4Info, p4Suit, p4Face, p4Name); }
            else if (inputController.p4Meanie) { ChangeToMeanie(p4Info, p4Suit, p4Face, p4Name); }
            else { ChangeSuit(p4Info, p4Suit, p4Suits, p4Face, p4Name, inputController.p4Up); }
        }
        
        // change player face
        if (p1Info.isJoined && !p1Info.isFinished && (inputController.GetUnreadLeft(1) || inputController.GetUnreadRight(1)))
        {
            if (inputController.p1YV) { ChangeToVY(p1Info, p1Suit, p1Face, p1Name); }
            else if (inputController.p1Meanie) { ChangeToMeanie(p1Info, p1Suit, p1Face, p1Name); }
            else { ChangeFace(p1Info, p1Face, p1Name, p1Suit, p1Suits, inputController.p1Right); }
        }
        if (p2Info.isJoined && !p2Info.isFinished && (inputController.GetUnreadLeft(2) || inputController.GetUnreadRight(2)))
        {
            if (inputController.p2YV) { ChangeToVY(p2Info, p2Suit, p2Face, p2Name); }
            else if (inputController.p2Meanie) { ChangeToMeanie(p2Info, p2Suit, p2Face, p2Name); }
            else { ChangeFace(p2Info, p2Face, p2Name, p2Suit, p2Suits, inputController.p2Right); }
        }
        if (p3Info.isJoined && !p3Info.isFinished && (inputController.GetUnreadLeft(3) || inputController.GetUnreadRight(3)))
        {
            if (inputController.p3YV) { ChangeToVY(p3Info, p3Suit, p3Face, p3Name); }
            else if (inputController.p3Meanie) { ChangeToMeanie(p3Info, p3Suit, p3Face, p3Name); }
            else { ChangeFace(p3Info, p3Face, p3Name, p3Suit, p3Suits, inputController.p3Right); }
        }
        if (p4Info.isJoined && !p4Info.isFinished && (inputController.GetUnreadLeft(4) || inputController.GetUnreadRight(4)))
        {
            if (inputController.p4YV) { ChangeToVY(p4Info, p4Suit, p4Face, p4Name); }
            else if (inputController.p4Meanie) { ChangeToMeanie(p4Info, p4Suit, p4Face, p4Name); }
            else { ChangeFace(p4Info, p4Face, p4Name, p4Suit, p4Suits, inputController.p4Right); }
        }


        // selected player
        if (p1Info.isJoined && !p1Info.isFinished && Input.GetButtonDown("A_1"))
        {
            MakePlayerSelection(p1Arts, p1Name, p1Info, "P1", 1);
        }
        if (p2Info.isJoined && !p2Info.isFinished && Input.GetButtonDown("A_2"))
        {
            MakePlayerSelection(p2Arts, p2Name, p2Info, "P2", 2);
        }
        if (p3Info.isJoined && !p3Info.isFinished && Input.GetButtonDown("A_3"))
        {
            MakePlayerSelection(p3Arts, p3Name, p3Info, "P3", 3);
        }
        if (p4Info.isJoined && !p4Info.isFinished && Input.GetButtonDown("A_4"))
        {
            MakePlayerSelection(p4Arts, p4Name, p4Info, "P4", 4);
        }

        // join game
        if (!p1Info.isJoined && Input.GetButtonDown("A_1"))
        {
            AudioController.Play("selectedthing");
            p1Info.isJoined = true;
            p1JoinA.SetActive(false);
            p1Name.enabled = true;
            p1Arts.SetActive(true);
        }
        if (!p2Info.isJoined && Input.GetButtonDown("A_2"))
        {
            AudioController.Play("selectedthing");
            p2Info.isJoined = true;
            p2JoinA.SetActive(false);
            p2Name.enabled = true;
            p2Arts.SetActive(true);
        }
        if (!p3Info.isJoined && Input.GetButtonDown("A_3"))
        {
            AudioController.Play("selectedthing");
            p3Info.isJoined = true;
            p3JoinA.SetActive(false);
            p3Name.enabled = true;
            p3Arts.SetActive(true);
        }
        if (!p4Info.isJoined && Input.GetButtonDown("A_4"))
        {
            AudioController.Play("selectedthing");
            p4Info.isJoined = true;
            p4JoinA.SetActive(false);
            p4Name.enabled = true;
            p4Arts.SetActive(true);
        }
    }

    private void MakePlayerSelection(GameObject playerArts, SpriteRenderer playerName, PlayerInfo playerInfo, String tag, int number)
    {
        AudioController.Play("selectedthing");
        playerInfo.isFinished = true;
        playerArts.SetActive(false);
        playerName.enabled = false;
        GameObject player = Instantiate(playerPrefab, playerArts.transform.position, Quaternion.identity) as GameObject;
        player.GetComponent<SpriteRenderer>().sprite = playerInfo.suit;
        player.transform.Find("playerFace").GetComponent<PlayerBlink>().SetFace(playerInfo.face, playerInfo.faceBlink);
        player.tag = tag;
        player.GetComponent<Shoot>().bullet = globalController.bullets[number - 1];
        globalController.AddPlayer(number, tag, playerInfo);
    }

    private void ChangeSuit(PlayerInfo playerInfo, SpriteRenderer playerRenderer, Sprite[] playerSuits, SpriteRenderer faceRenderer, SpriteRenderer playerName, bool incrementButtonPushed)
    {
        AudioController.Play("selectthing");
        if (incrementButtonPushed)
        {
            playerInfo.suitChoice = (playerInfo.suitChoice + 1 < p1Suits.Length) ? playerInfo.suitChoice + 1 : 0;
        }
        else
        {
            playerInfo.suitChoice = (playerInfo.suitChoice - 1 >= 0) ? playerInfo.suitChoice - 1 : p1Suits.Length - 1;
        }
        playerInfo.suit = playerSuits[playerInfo.suitChoice];
        playerRenderer.sprite = playerInfo.suit;
        ReApplyFaceName(playerInfo, faceRenderer, playerName); // do this in case we were on secret character before
    }

    private void ReApplyFaceName(PlayerInfo playerInfo, SpriteRenderer faceRenderer, SpriteRenderer playerName)
    {
        playerInfo.face = playerDetails[playerInfo.detailChoice].face;
        playerInfo.faceBlink = playerDetails[playerInfo.detailChoice].faceBlink;
        playerInfo.name = playerDetails[playerInfo.detailChoice].name;
        playerName.sprite = playerDetails[playerInfo.detailChoice].nameSprite;
        faceRenderer.sprite = playerInfo.face;
    }

    private void ChangeFace(PlayerInfo playerInfo, SpriteRenderer faceRenderer, SpriteRenderer playerName, SpriteRenderer playerRenderer, Sprite[] playerSuits, bool incrementButtonPushed) 
    {
        AudioController.Play("selectthing");
        if (incrementButtonPushed)
        {
            playerInfo.detailChoice = (playerInfo.detailChoice + 1 < playerDetails.Length) ? playerInfo.detailChoice + 1 : 0;
        }
        else
        {
            playerInfo.detailChoice = (playerInfo.detailChoice - 1 >= 0) ? playerInfo.detailChoice - 1 : playerDetails.Length - 1;
        }
        playerInfo.face = playerDetails[playerInfo.detailChoice].face;
        playerInfo.faceBlink = playerDetails[playerInfo.detailChoice].faceBlink;
        playerInfo.name = playerDetails[playerInfo.detailChoice].name;
        playerName.sprite = playerDetails[playerInfo.detailChoice].nameSprite;
        faceRenderer.sprite = playerInfo.face;
        ReApplySuit(playerInfo, playerRenderer, playerSuits); // do this in case we were on secret character before
    }

    private void ReApplySuit(PlayerInfo playerInfo, SpriteRenderer playerRenderer, Sprite[] playerSuits)
    {
        playerInfo.suit = playerSuits[playerInfo.suitChoice];
        playerRenderer.sprite = playerInfo.suit;
    }

    private void ChangeToVY(PlayerInfo playerInfo, SpriteRenderer suit, SpriteRenderer face, SpriteRenderer name)
    {
        AudioController.Play("selectthing");
        suit.sprite = yvSuit;
        face.sprite = yvDetail.face;
        name.sprite = yvDetail.nameSprite;
        playerInfo.suit = yvSuit;
        playerInfo.face = yvDetail.face;
        playerInfo.faceBlink = yvDetail.faceBlink;
        playerInfo.name = yvDetail.name;
    }

    private void ChangeToMeanie(PlayerInfo playerInfo, SpriteRenderer suit, SpriteRenderer face, SpriteRenderer name)
    {
        AudioController.Play("selectthing");
        suit.sprite = meanieSuit;
        face.sprite = meanieDetail.face;
        name.sprite = meanieDetail.nameSprite;
        playerInfo.suit = meanieSuit;
        playerInfo.face = meanieDetail.face;
        playerInfo.faceBlink = meanieDetail.faceBlink;
        playerInfo.name = meanieDetail.name;
    }

    private bool StartTimer()
    {
        int actives = 0;
        if (p1Info.isJoined && !p1Info.isFinished)
        {
            timer.StopTimer();
            timerStarted = false;
            return false;
        }
        if (p2Info.isJoined && !p2Info.isFinished)
        {
            timer.StopTimer();
            timerStarted = false;
            return false;
        }
        if (p3Info.isJoined && !p3Info.isFinished)
        {
            timer.StopTimer();
            timerStarted = false;
            return false;
        }
        if (p4Info.isJoined && !p4Info.isFinished)
        {
            timer.StopTimer();
            timerStarted = false;
            return false;
        }

        if (p1Info.isFinished)
        {
            actives++;
        }
        if (p2Info.isFinished)
        {
            actives++;
        }
        if (p3Info.isFinished)
        {
            actives++;
        }
        if (p4Info.isFinished)
        {
            actives++;
        }

        return actives >= 2;
    }
}