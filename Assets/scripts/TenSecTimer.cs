﻿using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TenSecTimer : MonoBehaviour
{
    public SpriteRenderer[] timerTexts;
    public Sprite[] timeSprites;
    public float timerLength;
    private float currentTime;
    private bool started, isReady;

    public void StartTimer()
    {
        started = true;
        currentTime = timerLength;
        foreach (SpriteRenderer t in timerTexts)
        {
            t.enabled = true;
        }
    }

    public void StopTimer()
    {
        started = false;
        currentTime = timerLength;
        foreach (SpriteRenderer t in timerTexts)
        {
            t.enabled = false;
        }
    }

    public bool IsReady()
    {
        return isReady;
    }

    private void Update()
    {
        if (started)
        {
            if ((currentTime -= Time.deltaTime) < -1)
            {
                isReady = true;
                return;
            }
            foreach (SpriteRenderer t in timerTexts)
            {
                t.sprite = timeSprites[Mathf.CeilToInt(currentTime)];
            }
        }
    }
}