﻿using System;
using System.Collections;
using Prime31.GoKitLite;
using UnityEngine;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    private GlobalController global;
    public GameObject playerPrefab;
    public Transform[] spawnPositions;
    private int playerCount;
    private bool levelStarted, gameOver;

    public ScoreboardController scoreboard;

    private void Start()
    {
        global = GameObject.FindGameObjectWithTag("GlobalController").GetComponent<GlobalController>();
        SpawnPlayers();
        levelStarted = true;
    }

    private void Update()
    {
        if (levelStarted && gameOver)
        {
            AudioController.Play("winner");
            levelStarted = false;
            EndGame();
        } 
        else if (levelStarted && playerCount == 1)
        {
            AudioController.Play("winner");
            levelStarted = false;
            EndLevel();
        }

        if (!levelStarted) // we're done with level
        {
            if (scoreboard.ScoreboardDone)
            {
                if (gameOver) // we're done with the game, back to logo
                {
                    Application.LoadLevel("logo");
                }
                else // time for next level
                {
                    global.MakePlayersAliveForNextRound();
                    string levelName = global.GetNextLevelName();
                    Application.LoadLevel(levelName);
                }
            }
        }
    }

    private void SpawnPlayers()
    {
        List<GlobalController.Player> players = global.GetPlayerList();
        foreach (GlobalController.Player player in players)
        {
            GameObject playerGO =Instantiate(playerPrefab, spawnPositions[player.Number - 1].position, Quaternion.identity) as GameObject;
            playerGO.GetComponent<SpriteRenderer>().sprite = player.Suit;
            playerGO.transform.Find("playerFace").GetComponent<SpriteRenderer>().sprite = player.Face;
            playerGO.GetComponent<Shoot>().bullet = player.Bullet;
            playerGO.tag = player.Tag;
        }

        playerCount = players.Count;
    }

    public void KillPlayer(String killedTag, String killerTag)
    {
        // check if someone hit the 10 kills to end the game
        if (global.AddToKills(killerTag))
        {
            gameOver = true;
        }

        global.KillPlayer(killedTag);
        playerCount--;
    }

    public void SuicidePlayer(String killedTag)
    {
        global.AddToSuicides(killedTag);
        global.KillPlayer(killedTag);
        playerCount--;
    }

    private void EndLevel()
    {
        // stop player movement and controls
        GameObject winner = GameObject.FindGameObjectWithTag(global.GetWinnerTag());
        winner.GetComponent<Jetpack>().CanSpin = false;
        winner.GetComponent<Jetpack>().CanBoost = false;
        winner.GetComponent<Shoot>().CanShoot = false;
        winner.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        // zoom in on winner
        //Camera.main.transform.GetComponent<PixelPerfectCamera>().enabled = false;
        //float size = Camera.main.orthographicSize;
        //System.Action<Transform, float> action = (trans, dt) => { trans.GetComponent<Camera>().orthographicSize = size - ((size - 1.5f) * dt); };
        //GoKitLite.instance.customAction(Camera.main.transform, 1f, action, 0f, GoKitLiteEasing.Cubic.EaseInOut);
        //GoKitLite.instance.positionTo(Camera.main.transform, 1f, winner.transform.position - (Vector3.forward * 1f)).setEaseFunction(GoKitLiteEasing.Cubic.EaseInOut);

        //StartCoroutine(WaitforWinEasing());

        scoreboard.StartScoreboard();
    }

    private void EndGame()
    {
        // GameObject winner = GameObject.FindGameObjectWithTag(global.GetGameWinnerTag());

        // stop all player movement and controls
        global.HaltAllPlayers();

        // zoom in on winner
        //System.Action<Transform, float> action = (trans, dt) => { trans.GetComponent<Camera>().orthographicSize = 5.2f - (3.7f * dt); };
        //GoKitLite.instance.customAction(Camera.main.transform, 1f, action, 0f, GoKitLiteEasing.Cubic.EaseInOut);
        //GoKitLite.instance.positionTo(Camera.main.transform, 1f, winner.transform.position - (Vector3.forward * 1f)).setEaseFunction(GoKitLiteEasing.Cubic.EaseInOut);

        //StartCoroutine(WaitforWinEasing());

        scoreboard.StartScoreboard();
    }

    private IEnumerator WaitforWinEasing()
    {
        yield return new WaitForSeconds(3.0f);
        scoreboard.StartScoreboard();
    }

    private IEnumerator WaitforGameOverEasing()
    {
        yield return new WaitForSeconds(3.0f);
        // todo: show off end of game
    }
}