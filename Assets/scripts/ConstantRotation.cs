﻿using UnityEngine;
using System.Collections;

public class ConstantRotation : MonoBehaviour
{
    public float speed;

    private void Update()
    {
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);
    }
}