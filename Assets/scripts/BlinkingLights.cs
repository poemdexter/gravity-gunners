﻿using Prime31.GoKitLite;
using UnityEngine;

public class BlinkingLights : MonoBehaviour
{
    public float power;
    private const float TIME = 2f;

    private float currentTime;

    private void Start()
    {
        currentTime = TIME;
    }

    private void Update()
    {
        if ((currentTime += Time.deltaTime) > TIME)
        {
            Blink();
            currentTime = 0;
        }
    }

    private void Blink()
    {
        var flow = new TweenFlow()
            .add(0f, () => GoKitLite.instance.customAction(transform, 1f, (trans, t) => { GetComponent<Light>().intensity = t * power; }, 0f, GoKitLiteEasing.Quadratic.EaseOut))
            .add(1f, () => GoKitLite.instance.customAction(transform, 1f, (trans, t) => { GetComponent<Light>().intensity = (1 - t) * power; }, 0f, GoKitLiteEasing.Quadratic.EaseOut));
        StartCoroutine(flow.start());
    }
}