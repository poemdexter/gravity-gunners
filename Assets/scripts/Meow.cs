﻿using UnityEngine;
using System.Collections;

public class Meow : MonoBehaviour 
{
    private void Update()
    {
        if (CompareTag("P1") && Input.GetButtonDown("B_1"))
        {
            Noise();
        }
        if (CompareTag("P2") && Input.GetButtonDown("B_2"))
        {
            Noise();
        }
        if (CompareTag("P3") && Input.GetButtonDown("B_3"))
        {
            Noise();
        }
        if (CompareTag("P4") && Input.GetButtonDown("B_4"))
        {
            Noise();
        }
    }

    private void Noise()
    {
        AudioController.Play("meow");
    }
}
