﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour 
{
    public bool p1Up, p1Down, p1Left, p1Right;
    public bool p2Up, p2Down, p2Left, p2Right;
    public bool p3Up, p3Down, p3Left, p3Right;
    public bool p4Up, p4Down, p4Left, p4Right;

    public bool p1UpRead, p1DownRead, p1LeftRead, p1RightRead;
    public bool p2UpRead, p2DownRead, p2LeftRead, p2RightRead;
    public bool p3UpRead, p3DownRead, p3LeftRead, p3RightRead;
    public bool p4UpRead, p4DownRead, p4LeftRead, p4RightRead;

    public bool p1YV, p2YV, p3YV, p4YV;
    public bool p1Meanie, p2Meanie, p3Meanie, p4Meanie;

    private string[] yvCode =     new string[] { "up", "up", "down", "down", "up", "down", "up", "down" };
    private string[] meanieCode = new string[] { "up", "down", "left", "right", "up", "down", "left", "right" };
    private int p1YVPos, p2YVPos, p3YVPos, p4YVPos;
    private int p1MeaniePos, p2MeaniePos, p3MeaniePos, p4MeaniePos;

    void Update()
    {
        DPadInput(); // console PC
        HandleArcadeButtons(); // arcade only
        StickInput(); // console PC
        TryResetReads();
    }

    public bool GetUnreadUp(int player)
    {
        switch(player)
        {
            case 1:
                if(!p1UpRead && p1Up)
                {
                    p1UpRead = true;
                    DetectSecretCodes(1);
                    return true;
                }
                return false;
            case 2:
                if(!p2UpRead && p2Up)
                {
                    p2UpRead = true;
                    DetectSecretCodes(2);
                    return true;
                }
                return false;
            case 3:
                if(!p3UpRead && p3Up)
                {
                    p3UpRead = true;
                    DetectSecretCodes(3);
                    return true;
                }
                return false;
            case 4:
                if(!p4UpRead && p4Up)
                {
                    p4UpRead = true;
                    DetectSecretCodes(4);
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    public bool GetUnreadDown(int player)
    {
        switch (player)
        {
            case 1:
                if (!p1DownRead && p1Down)
                {
                    p1DownRead = true;
                    DetectSecretCodes(1);
                    return true;
                }
                return false;
            case 2:
                if (!p2DownRead && p2Down)
                {
                    p2DownRead = true;
                    DetectSecretCodes(2);
                    return true;
                }
                return false;
            case 3:
                if (!p3DownRead && p3Down)
                {
                    p3DownRead = true;
                    DetectSecretCodes(3);
                    return true;
                }
                return false;
            case 4:
                if (!p4DownRead && p4Down)
                {
                    p4DownRead = true;
                    DetectSecretCodes(4);
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    public bool GetUnreadRight(int player)
    {
        switch (player)
        {
            case 1:
                if (!p1RightRead && p1Right)
                {
                    p1RightRead = true;
                    DetectSecretCodes(1);
                    return true;
                }
                return false;
            case 2:
                if (!p2RightRead && p2Right)
                {
                    p2RightRead = true;
                    DetectSecretCodes(2);
                    return true;
                }
                return false;
            case 3:
                if (!p3RightRead && p3Right)
                {
                    p3RightRead = true;
                    DetectSecretCodes(3);
                    return true;
                }
                return false;
            case 4:
                if (!p4RightRead && p4Right)
                {
                    p4RightRead = true;
                    DetectSecretCodes(4);
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    public bool GetUnreadLeft(int player)
    {
        switch (player)
        {
            case 1:
                if (!p1LeftRead && p1Left)
                {
                    p1LeftRead = true;
                    DetectSecretCodes(1);
                    return true;
                }
                return false;
            case 2:
                if (!p2LeftRead && p2Left)
                {
                    p2LeftRead = true;
                    DetectSecretCodes(2);
                    return true;
                }
                return false;
            case 3:
                if (!p3LeftRead && p3Left)
                {
                    p3LeftRead = true;
                    DetectSecretCodes(3);
                    return true;
                }
                return false;
            case 4:
                if (!p4LeftRead && p4Left)
                {
                    p4LeftRead = true;
                    DetectSecretCodes(4);
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    private void HandleArcadeButtons()
    {
        if (Input.GetButtonDown("P1_Right"))
        {
            p1Right = true;
        }
        else if (Input.GetButtonUp("P1_Right"))
        {
            p1Right = false;
        }
        if (Input.GetButtonDown("P1_Left"))
        {
            p1Left = true;
        }
        else if (Input.GetButtonUp("P1_Left"))
        {
            p1Left = false;
        }
        if (Input.GetButtonDown("P1_Up"))
        {
            p1Up = true;
        }
        else if (Input.GetButtonUp("P1_Up"))
        {
            p1Up = false;
        }
        if (Input.GetButtonDown("P1_Down"))
        {
            p1Down = true;
        }
        else if (Input.GetButtonUp("P1_Down"))
        {
            p1Down = false;
        }

        if (Input.GetButtonDown("P2_Right"))
        {
            p2Right = true;
        }
        else if (Input.GetButtonUp("P2_Right"))
        {
            p2Right = false;
        }
        if (Input.GetButtonDown("P2_Left"))
        {
            p2Left = true;
        }
        else if (Input.GetButtonUp("P2_Left"))
        {
            p2Left = false;
        }
        if (Input.GetButtonDown("P2_Up"))
        {
            p2Up = true;
        }
        else if (Input.GetButtonUp("P2_Up"))
        {
            p2Up = false;
        }
        if (Input.GetButtonDown("P2_Down"))
        {
            p2Down = true;
        }
        else if (Input.GetButtonUp("P2_Down"))
        {
            p2Down = false;
        }

        if (Input.GetButtonDown("P3_Right"))
        {
            p3Right = true;
        }
        else if (Input.GetButtonUp("P3_Right"))
        {
            p3Right = false;
        }
        if (Input.GetButtonDown("P3_Left"))
        {
            p3Left = true;
        }
        else if (Input.GetButtonUp("P3_Left"))
        {
            p3Left = false;
        }
        if (Input.GetButtonDown("P3_Up"))
        {
            p3Up = true;
        }
        else if (Input.GetButtonUp("P3_Up"))
        {
            p3Up = false;
        }
        if (Input.GetButtonDown("P3_Down"))
        {
            p3Down = true;
        }
        else if (Input.GetButtonUp("P3_Down"))
        {
            p3Down = false;
        }

        if (Input.GetButtonDown("P4_Right"))
        {
            p4Right = true;
        }
        else if (Input.GetButtonUp("P4_Right"))
        {
            p4Right = false;
        }
        if (Input.GetButtonDown("P4_Left"))
        {
            p4Left = true;
        }
        else if (Input.GetButtonUp("P4_Left"))
        {
            p4Left = false;
        }
        if (Input.GetButtonDown("P4_Up"))
        {
            p4Up = true;
        }
        else if (Input.GetButtonUp("P4_Up"))
        {
            p4Up = false;
        }
        if (Input.GetButtonDown("P4_Down"))
        {
            p4Down = true;
        }
        else if (Input.GetButtonUp("P4_Down"))
        {
            p4Down = false;
        }
    }

    private void DPadInput()
    {
        // analog
        if (Input.GetAxisRaw("DPad_XAxis_1") > 0) // right
        {
            p1Right = true;
            p1Left = false;
        }
        else if (Input.GetAxisRaw("DPad_XAxis_1") < 0) // left
        {
            p1Right = false;
            p1Left = true;
        }
        else
        {
            p1Right = false;
            p1Left = false;
        }
        if (Input.GetAxisRaw("DPad_YAxis_1") > 0) // up
        {
            p1Up = true;
            p1Down = false;
        }
        else if (Input.GetAxisRaw("DPad_YAxis_1") < 0) // down
        {
            p1Up = false;
            p1Down = true;
        }
        else
        {
            p1Up = false;
            p1Down = false;
        }
        
        // analog
        if (Input.GetAxisRaw("DPad_XAxis_2") > 0) // right
        {
            p2Right = true;
            p2Left = false;
        }
        else if (Input.GetAxisRaw("DPad_XAxis_2") < 0) // left
        {
            p2Right = false;
            p2Left = true;
        }
        else
        {
            p2Right = false;
            p2Left = false;
        }
        if (Input.GetAxisRaw("DPad_YAxis_2") > 0) // up
        {
            p2Up = true;
            p2Down = false;
        }
        else if (Input.GetAxisRaw("DPad_YAxis_2") < 0) // down
        {
            p2Up = false;
            p2Down = true;
        }
        else
        {
            p2Up = false;
            p2Down = false;
        }
        
        // analog
        if (Input.GetAxisRaw("DPad_XAxis_3") > 0) // right
        {
            p3Right = true;
            p3Left = false;
        }
        else if (Input.GetAxisRaw("DPad_XAxis_3") < 0) // left
        {
            p3Right = false;
            p3Left = true;
        }
        else
        {
            p3Right = false;
            p3Left = false;
        }
        if (Input.GetAxisRaw("DPad_YAxis_3") > 0) // up
        {
            p3Up = true;
            p3Down = false;
        }
        else if (Input.GetAxisRaw("DPad_YAxis_3") < 0) // down
        {
            p3Up = false;
            p3Down = true;
        }
        else
        {
            p3Up = false;
            p3Down = false;
        }
        
        // analog
        if (Input.GetAxisRaw("DPad_XAxis_4") > 0) // right
        {
            p4Right = true;
            p4Left = false;
        }
        else if (Input.GetAxisRaw("DPad_XAxis_4") < 0) // left
        {
            p4Right = false;
            p4Left = true;
        }
        else
        {
            p4Right = false;
            p4Left = false;
        }
        if (Input.GetAxisRaw("DPad_YAxis_4") > 0) // up
        {
            p4Up = true;
            p4Down = false;
        }
        else if (Input.GetAxisRaw("DPad_YAxis_4") < 0) // down
        {
            p4Up = false;
            p4Down = true;
        }
        else
        {
            p4Up = false;
            p4Down = false;
        }
    }

    private void StickInput()
    {
        if (Input.GetAxisRaw("L_XAxis_1") != 0 || Input.GetAxisRaw("L_YAxis_1") != 0)
        {
            Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_1"), Input.GetAxis("L_YAxis_1"));
            float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
            AssignP1Directions(CheckAngle(angle));
        }
        
        if (Input.GetAxisRaw("L_XAxis_2") != 0 || Input.GetAxisRaw("L_YAxis_2") != 0)
        {
            Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_2"), Input.GetAxis("L_YAxis_2"));
            float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
            AssignP2Directions(CheckAngle(angle));
        }
        
        if (Input.GetAxisRaw("L_XAxis_3") != 0 || Input.GetAxisRaw("L_YAxis_3") != 0)
        {
            Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_3"), Input.GetAxis("L_YAxis_3"));
            float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
            AssignP3Directions(CheckAngle(angle));
        }
        
        if (Input.GetAxisRaw("L_XAxis_4") != 0 || Input.GetAxisRaw("L_YAxis_4") != 0)
        {
            Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_4"), Input.GetAxis("L_YAxis_4"));
            float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
            AssignP4Directions(CheckAngle(angle));
        }
    }

    private bool[] CheckAngle(float angle)
    {
        // up, down, left, right
        bool[] directions = new bool[4];

        if (angle >= 0 && angle <= 22.5)
        {
            directions[1] = true;
        }
        else if (angle > 22.5 && angle <= 67.5)
        {
            directions[1] = true;
            directions[3] = true;
        }
        else if (angle > 67.5 && angle <= 112.5)
        {
            directions[3] = true;
        }
        else if (angle > 112.5 && angle <= 157.5)
        {
            directions[0] = true;
            directions[3] = true;
        }
        else if (angle > 157.5 && angle <= 180)
        {
            directions[0] = true;
        }
        else if (angle <= 0 && angle >= -22.5)
        {
            directions[1] = true;
        }
        else if (angle < -22.5 && angle >= -67.5)
        {
            directions[1] = true;
            directions[2] = true;
        }
        else if (angle < -67.5 && angle >= -112.5)
        {
            directions[2] = true;
        }
        else if (angle < -112.5 && angle >= -157.5)
        {
            directions[0] = true;
            directions[2] = true;
        }
        else if (angle < -157.5 && angle >= -180)
        {
            directions[0] = true;
        }
        return directions;
    }

    private void AssignP1Directions(bool[] directions)
    {
        p1Up = directions[0];
        p1Down = directions[1];
        p1Left = directions[2];
        p1Right = directions[3];
    }

    private void AssignP2Directions(bool[] directions)
    {
        p2Up = directions[0];
        p2Down = directions[1];
        p2Left = directions[2];
        p2Right = directions[3];
    }

    private void AssignP3Directions(bool[] directions)
    {
        p3Up = directions[0];
        p3Down = directions[1];
        p3Left = directions[2];
        p3Right = directions[3];
    }

    private void AssignP4Directions(bool[] directions)
    {
        p4Up = directions[0];
        p4Down = directions[1];
        p4Left = directions[2];
        p4Right = directions[3];
    }

    private void TryResetReads()
    {
        if (!p1Up) { p1UpRead = false; }
        if (!p1Down) { p1DownRead = false; }
        if (!p1Left) { p1LeftRead = false; }
        if (!p1Right) { p1RightRead = false; }

        if (!p2Up) { p2UpRead = false; }
        if (!p2Down) { p2DownRead = false; }
        if (!p2Left) { p2LeftRead = false; }
        if (!p2Right) { p2RightRead = false; }

        if (!p3Up) { p3UpRead = false; }
        if (!p3Down) { p3DownRead = false; }
        if (!p3Left) { p3LeftRead = false; }
        if (!p3Right) { p3RightRead = false; }

        if (!p4Up) { p4UpRead = false; }
        if (!p4Down) { p4DownRead = false; }
        if (!p4Left) { p4LeftRead = false; }
        if (!p4Right) { p4RightRead = false; }
    }

    private void DetectSecretCodes(int player)
    {
        switch (player)
        {
            case 1:
                DetectSecretCodes(yvCode,     ref p1YV,     ref p1YVPos,     p1UpRead, p1DownRead, p1LeftRead, p1RightRead);
                DetectSecretCodes(meanieCode, ref p1Meanie, ref p1MeaniePos, p1UpRead, p1DownRead, p1LeftRead, p1RightRead);
                break;
            case 2:
                DetectSecretCodes(yvCode,     ref p2YV,     ref p2YVPos,     p2UpRead, p2DownRead, p2LeftRead, p2RightRead);
                DetectSecretCodes(meanieCode, ref p2Meanie, ref p2MeaniePos, p2UpRead, p2DownRead, p2LeftRead, p2RightRead);
                break;
            case 3:
                DetectSecretCodes(yvCode,     ref p3YV,     ref p3YVPos,     p3UpRead, p3DownRead, p3LeftRead, p3RightRead);
                DetectSecretCodes(meanieCode, ref p3Meanie, ref p3MeaniePos, p3UpRead, p3DownRead, p3LeftRead, p3RightRead);
                break;
            case 4:
                DetectSecretCodes(yvCode,     ref p4YV,     ref p4YVPos,     p4UpRead, p4DownRead, p4LeftRead, p4RightRead);
                DetectSecretCodes(meanieCode, ref p4Meanie, ref p4MeaniePos, p4UpRead, p4DownRead, p4LeftRead, p4RightRead);
                break;
            default: break;
        }
    }
    private void DetectSecretCodes(string[] code, ref bool complete, ref int codePos, bool up, bool down, bool left, bool right)
    {
        if (codePos != code.Length)
        {
            switch (code[codePos])
            {
                case "up":
                    if (left || right || down) // bad input
                    {
                        codePos = 0;
                    }
                    else if (up) // good input
                    {
                        if (++codePos == code.Length) // inputs done
                        {
                            complete = true;
                        }
                    }
                    break;
                case "down":
                    if (left || right || up) // bad input
                    {
                        codePos = 0;
                    }
                    else if (down) // good input
                    {
                        if (++codePos == code.Length)
                        {
                            complete = true; // inputs done
                        }
                    }
                    break;
                case "left":
                    if (down || right || up) // bad input
                    {
                        codePos = 0;
                    }
                    else if (left) // good input
                    {
                        if (++codePos == code.Length)
                        {
                            complete = true; // inputs done
                        }
                    }
                    break;
                case "right":
                    if (left || down || up) // bad input
                    {
                        codePos = 0;
                    }
                    else if (right) // good input
                    {
                        if (++codePos == code.Length)
                        {
                            complete = true; // inputs done
                        }
                    }
                    break;
                default: break;
            }
        }
        else  // more input after code done
        {
            codePos = 0;
            complete = false;
        }
    }
}
