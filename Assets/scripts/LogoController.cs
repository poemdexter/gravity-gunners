﻿using UnityEngine;
using System.Collections;

public class LogoController : MonoBehaviour
{
    public double inOutTime;
    public double stayTime;
    public SpriteRenderer logoSprite;
    private double currentIdleTime;
    public bool isStay, isFadeOut;

    private void Awake()
    {
        Cursor.visible = false;
    }

    private void Start()
    {
        // if global exists here in logo, it means the game is over and we started demo over again
        var global = GameObject.FindGameObjectWithTag("GlobalController");
        if (global != null)
        {
            Destroy(global);
        }
        // if audio controller exists here, destroy it so we can start anew
        var audioController = GameObject.FindGameObjectWithTag("AudioController");
        if (audioController != null)
        {
            Destroy(audioController);
        }
    }

    private void Update()
    {
        if (isFadeOut) // fade out
        {
            if ((currentIdleTime += Time.deltaTime) < inOutTime)
            {
                logoSprite.color = new Color(1f, 1f, 1f, 1f - (float)(currentIdleTime / inOutTime));
            }
            else
            {
                Application.LoadLevel("title");
            }
        }
        else if (isStay) // stay
        {
            if ((currentIdleTime += Time.deltaTime) > stayTime)
            {
                currentIdleTime = 0;
                isFadeOut = true;
            }
        }
        else // fade in
        {
            if ((currentIdleTime += Time.deltaTime) < inOutTime)
            {
                logoSprite.color = new Color(1f, 1f, 1f, (float)(currentIdleTime / inOutTime));
            }
            else
            {
                currentIdleTime = 0;
                isStay = true;
                logoSprite.color = new Color(1f, 1f, 1f, 1f);
            }
        }
    }
}