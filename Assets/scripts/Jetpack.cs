﻿using UnityEngine;
using System.Collections;

public class Jetpack : MonoBehaviour
{
    public float spinSpeed;
    private float currentSpeed;
    public bool CanSpin { get; set; }
    public bool CanBoost { get; set; }
    private bool boosting, boosted;
    private Vector3 boostDir;
    public float boostLength;
    public float boostSpeed;
    private float currentBoostTime;
    private bool up, down, left, right;
    public float boostDelay;
    private float currentDelayTime = 5f;

    private bool p1Up, p1Down, p1Left, p1Right;
    private bool p2Up, p2Down, p2Left, p2Right;
    private bool p3Up, p3Down, p3Left, p3Right;
    private bool p4Up, p4Down, p4Left, p4Right;

    private bool spinLeft, spinRight, changeSpin;

    private float spinDirection = 1f;

    private void Start()
    {
        CanSpin = true;
        CanBoost = true;
        currentSpeed = spinSpeed;
        currentDelayTime = boostDelay;
    }

    private void Update()
    {
        HandleArcadeButtons();
        DPadInput();
        StickInput();
        ChangeSpinInput();

        // boosting
        if ((currentDelayTime += Time.deltaTime) > boostDelay)
        {
            if (up || down || left || right)
            {
                boostDir = new Vector2();
                if (up)
                {
                    boostDir += new Vector3(0, 1, 0);
                }
                if (down)
                {
                    boostDir += new Vector3(0, -1, 0);
                }
                if (left)
                {
                    boostDir += new Vector3(-1, 0, 0);
                }
                if (right)
                {
                    boostDir += new Vector3(1, 0, 0);
                }
                boostDir.Normalize();
                boosting = true;
                currentDelayTime = 0;
            }
            else if (spinLeft && spinDirection != -1f)
            {
                boosting = true;
                changeSpin = true;
                currentDelayTime = 0;
                spinDirection = -1f;
            }
            else if (spinRight && spinDirection != 1f)
            {
                boosting = true;
                changeSpin = true;
                currentDelayTime = 0;
                spinDirection = 1f;
            }
        }

        if (CanBoost && boosting)
        {
            if (!boosted)
            {
                if (!changeSpin)
                {
                    GetComponent<Rigidbody2D>().AddForce(boostDir * boostSpeed);
                }
                
                AudioController.Play("jetpack");
                boosted = true;
            }
            else
            {
                if ((currentBoostTime += Time.deltaTime) > boostLength)
                {
                    if (!changeSpin)
                    {
                        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    }
                    changeSpin = false;
                    boosted = false;
                    boosting = false;
                    currentBoostTime = 0;
                }
               
            }
        }

        // normal rotation
        if (CanSpin)
        {
            GetComponent<Rigidbody2D>().inertia = 0;
            transform.Rotate(Time.deltaTime * currentSpeed * Vector3.forward * spinDirection);
        }
    }

    private void ChangeSpinInput()
    {
        if (CompareTag("P1") && Input.GetAxisRaw("TriggersR_1") != 0)
        {
            spinRight = true;
            spinLeft = false;
        }
        else if (CompareTag("P1") && Input.GetAxisRaw("TriggersL_1") != 0)
        {
            spinLeft = true;
            spinRight = false;
        }
        else if (CompareTag("P2") && Input.GetAxisRaw("TriggersR_2") != 0)
        {
            spinRight = true;
            spinLeft = false;
        }
        else if (CompareTag("P2") && Input.GetAxisRaw("TriggersL_2") != 0)
        {
            spinLeft = true;
            spinRight = false;
        }
        else if (CompareTag("P3") && Input.GetAxisRaw("TriggersR_3") != 0)
        {
            spinRight = true;
            spinLeft = false;
        }
        else if (CompareTag("P3") && Input.GetAxisRaw("TriggersL_3") != 0)
        {
            spinLeft = true;
            spinRight = false;
        }
        else if (CompareTag("P4") && Input.GetAxisRaw("TriggersR_4") != 0)
        {
            spinRight = true;
            spinLeft = false;
        }
        else if (CompareTag("P4") && Input.GetAxisRaw("TriggersL_4") != 0)
        {
            spinLeft = true;
            spinRight = false;
        }
        else
        {
            spinRight = false;
            spinLeft = false;
        }
    }

    private void HandleArcadeButtons()
    {
        if (Input.GetButtonDown("P1_Right"))
        {
            p1Right = true;
        }
        else if (Input.GetButtonUp("P1_Right"))
        {
            p1Right = false;
        }
        if (Input.GetButtonDown("P1_Left"))
        {
            p1Left = true;
        }
        else if (Input.GetButtonUp("P1_Left"))
        {
            p1Left = false;
        }
        if (Input.GetButtonDown("P1_Up"))
        {
            p1Up = true;
        }
        else if (Input.GetButtonUp("P1_Up"))
        {
            p1Up = false;
        }
        if (Input.GetButtonDown("P1_Down"))
        {
            p1Down = true;
        }
        else if (Input.GetButtonUp("P1_Down"))
        {
            p1Down = false;
        }

        if (Input.GetButtonDown("P2_Right"))
        {
            p2Right = true;
        }
        else if (Input.GetButtonUp("P2_Right"))
        {
            p2Right = false;
        }
        if (Input.GetButtonDown("P2_Left"))
        {
            p2Left = true;
        }
        else if (Input.GetButtonUp("P2_Left"))
        {
            p2Left = false;
        }
        if (Input.GetButtonDown("P2_Up"))
        {
            p2Up = true;
        }
        else if (Input.GetButtonUp("P2_Up"))
        {
            p2Up = false;
        }
        if (Input.GetButtonDown("P2_Down"))
        {
            p2Down = true;
        }
        else if (Input.GetButtonUp("P2_Down"))
        {
            p2Down = false;
        }

        if (Input.GetButtonDown("P3_Right"))
        {
            p3Right = true;
        }
        else if (Input.GetButtonUp("P3_Right"))
        {
            p3Right = false;
        }
        if (Input.GetButtonDown("P3_Left"))
        {
            p3Left = true;
        }
        else if (Input.GetButtonUp("P3_Left"))
        {
            p3Left = false;
        }
        if (Input.GetButtonDown("P3_Up"))
        {
            p3Up = true;
        }
        else if (Input.GetButtonUp("P3_Up"))
        {
            p3Up = false;
        }
        if (Input.GetButtonDown("P3_Down"))
        {
            p3Down = true;
        }
        else if (Input.GetButtonUp("P3_Down"))
        {
            p3Down = false;
        }

        if (Input.GetButtonDown("P4_Right"))
        {
            p4Right = true;
        }
        else if (Input.GetButtonUp("P4_Right"))
        {
            p4Right = false;
        }
        if (Input.GetButtonDown("P4_Left"))
        {
            p4Left = true;
        }
        else if (Input.GetButtonUp("P4_Left"))
        {
            p4Left = false;
        }
        if (Input.GetButtonDown("P4_Up"))
        {
            p4Up = true;
        }
        else if (Input.GetButtonUp("P4_Up"))
        {
            p4Up = false;
        }
        if (Input.GetButtonDown("P4_Down"))
        {
            p4Down = true;
        }
        else if (Input.GetButtonUp("P4_Down"))
        {
            p4Down = false;
        }
    }

    private void DPadInput()
    {
        if (CompareTag("P1") && Input.GetButtonDown("X_1"))
        {
            // analog
            if (Input.GetAxisRaw("DPad_XAxis_1") > 0 || p1Right) // right
            {
                right = true;
            }
            else if (Input.GetAxisRaw("DPad_XAxis_1") < 0 || p1Left) // left
            {
                left = true;
            }
            if (Input.GetAxisRaw("DPad_YAxis_1") > 0 || p1Up) // up
            {
                up = true;
            }
            else if (Input.GetAxisRaw("DPad_YAxis_1") < 0 || p1Down) // down
            {
                down = true;
            }
        }
        else if (CompareTag("P2") && Input.GetButtonDown("X_2"))
        {
            // analog
            if (Input.GetAxisRaw("DPad_XAxis_2") > 0 || p2Right) // right
            {
                right = true;
            }
            else if (Input.GetAxisRaw("DPad_XAxis_2") < 0 || p2Left) // left
            {
                left = true;
            }
            if (Input.GetAxisRaw("DPad_YAxis_2") > 0 || p2Up) // up
            {
                up = true;
            }
            else if (Input.GetAxisRaw("DPad_YAxis_2") < 0 || p2Down) // down
            {
                down = true;
            }
        }
        else if (CompareTag("P3") && Input.GetButtonDown("X_3"))
        {
            // analog
            if (Input.GetAxisRaw("DPad_XAxis_3") > 0 || p3Right) // right
            {
                right = true;
            }
            else if (Input.GetAxisRaw("DPad_XAxis_3") < 0 || p3Left) // left
            {
                left = true;
            }
            if (Input.GetAxisRaw("DPad_YAxis_3") > 0 || p3Up) // up
            {
                up = true;
            }
            else if (Input.GetAxisRaw("DPad_YAxis_3") < 0 || p3Down) // down
            {
                down = true;
            }
        }
        else if (CompareTag("P4") && Input.GetButtonDown("X_4"))
        {
            // analog
            if (Input.GetAxisRaw("DPad_XAxis_4") > 0 || p4Right) // right
            {
                right = true;
            }
            else if (Input.GetAxisRaw("DPad_XAxis_4") < 0 || p4Left) // left
            {
                left = true;
            }
            if (Input.GetAxisRaw("DPad_YAxis_4") > 0 || p4Up) // up
            {
                up = true;
            }
            else if (Input.GetAxisRaw("DPad_YAxis_4") < 0 || p4Down) // down
            {
                down = true;
            }
        }
        else
        {
            up = false;
            down = false;
            left = false;
            right = false;
        }
    }

    private void StickInput()
    {
        if (CompareTag("P1") && Input.GetButtonDown("X_1"))
        {
            if (Input.GetAxisRaw("L_XAxis_1") != 0 || Input.GetAxisRaw("L_YAxis_1") != 0)
            {
                Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_1"), Input.GetAxis("L_YAxis_1"));
                float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
                CheckAngle(angle);
            }
        }
        else if (CompareTag("P2") && Input.GetButtonDown("X_2"))
        {
            if (Input.GetAxisRaw("L_XAxis_2") != 0 || Input.GetAxisRaw("L_YAxis_2") != 0)
            {
                Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_2"), Input.GetAxis("L_YAxis_2"));
                float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
                CheckAngle(angle);
            }
        }
        else if (CompareTag("P3") && Input.GetButtonDown("X_3"))
        {
            if (Input.GetAxisRaw("L_XAxis_3") != 0 || Input.GetAxisRaw("L_YAxis_3") != 0)
            {
                Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_3"), Input.GetAxis("L_YAxis_3"));
                float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
                CheckAngle(angle);
            }
        }
        else if (CompareTag("P4") && Input.GetButtonDown("X_4"))
        {
            if (Input.GetAxisRaw("L_XAxis_4") != 0 || Input.GetAxisRaw("L_YAxis_4") != 0)
            {
                Vector3 inputVector = new Vector3(Input.GetAxis("L_XAxis_4"), Input.GetAxis("L_YAxis_4"));
                float angle = Mathf.Atan2(inputVector.x, inputVector.y) * Mathf.Rad2Deg;
                CheckAngle(angle);
            }
        }
        else
        {
            up = false;
            down = false;
            left = false;
            right = false;
        }
    }

    private void CheckAngle(float angle)
    {
        if (angle >= 0 && angle <= 22.5)
        {
            down = true;
        }
        else if (angle > 22.5 && angle <= 67.5)
        {
            down = true;
            right = true;
        }
        else if (angle > 67.5 && angle <= 112.5)
        {
            right = true;
        }
        else if (angle > 112.5 && angle <= 157.5)
        {
            up = true;
            right = true;
        }
        else if (angle > 157.5 && angle <= 180)
        {
            up = true;
        }
        else if (angle <= 0 && angle >= -22.5)
        {
            down = true;
        }
        else if (angle < -22.5 && angle >= -67.5)
        {
            down = true;
            left = true;
        }
        else if (angle < -67.5 && angle >= -112.5)
        {
            left = true;
        }
        else if (angle < -112.5 && angle >= -157.5)
        {
            up = true;
            left = true;
        }
        else if (angle < -157.5 && angle >= -180)
        {
            up = true;
        }
    }
}