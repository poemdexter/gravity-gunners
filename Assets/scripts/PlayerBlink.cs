﻿using UnityEngine;
using System.Collections;

public class PlayerBlink : MonoBehaviour 
{
    public Sprite Blink { get; set; }
    private Sprite Unblink { get; set; }

    private double currentTime, currentBlinkTime;
    private float blinkLength = .2f;
    public float blinkWaitTime;
    private bool canBlink, isBlinking;

    public void SetFace(Sprite face, Sprite faceBlink)
    {
        GetComponent<SpriteRenderer>().sprite = face;
        Unblink = face;
        Blink = faceBlink;
        canBlink = true;
        blinkWaitTime = Random.Range(2f, 7f);
    }
	void Update() 
    {
        if (canBlink)
        {
            if (isBlinking && (currentBlinkTime += Time.deltaTime) > blinkLength)
            {
                isBlinking = false;
                currentBlinkTime = 0;
                GetComponent<SpriteRenderer>().sprite = Unblink;
            }
            else if ((currentTime += Time.deltaTime) > blinkWaitTime)
            {
                isBlinking = true;
                currentTime = 0;
                blinkWaitTime = Random.Range(2f, 7f);
                GetComponent<SpriteRenderer>().sprite = Blink;
            }
        }
	}
}
