﻿using System;
using UnityEngine;

public class Fired : MonoBehaviour
{
    public float speed;
    public GameObject smoke;

    private void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }
    
    private void OnCollisionEnter2D(Collision2D c)
    {
        if (c.collider.CompareTag("electric") || c.collider.CompareTag("station"))
        {
            Instantiate(smoke, transform.position, transform.rotation);

            // inform player that his/her bullet was destroyed so they can shoot another
            GameObject player = GameObject.FindGameObjectWithTag(GetComponent<Owner>().Owns);
            if (player != null)
            {
                player.GetComponent<Shoot>().BulletDestroyed();    
            }

            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.CompareTag("barrier"))
        {
            // inform player that his/her bullet was destroyed so they can shoot another
            GameObject player = GameObject.FindGameObjectWithTag(GetComponent<Owner>().Owns);
            if (player != null)
            {
                player.GetComponent<Shoot>().BulletDestroyed();
            }

            Destroy(gameObject);
        }
    }
}