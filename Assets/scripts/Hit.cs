﻿using UnityEngine;
using System.Collections;

public class Hit : MonoBehaviour
{
    private LevelManager levelManager;
    public ParticleSystem blood;

    private void Start()
    {
        if (GameObject.FindGameObjectWithTag("LevelManager") != null)
        {
            levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("bullet") && col.gameObject.GetComponent<Owner>().Owns != tag)
        {
            // inform player that his/her bullet was destroyed so they can shoot another
            GameObject player = GameObject.FindGameObjectWithTag(col.gameObject.GetComponent<Owner>().Owns);
            if (player != null)
            {
                player.GetComponent<Shoot>().BulletDestroyed();
            }

            Destroy(col.gameObject);
            FireBloodParticles();
            AudioController.Play("playerhit");
            levelManager.KillPlayer(tag, col.gameObject.GetComponent<Owner>().Owns);
        }
        else if (col.gameObject.CompareTag("electric"))
        {
            FireBloodParticles();
            AudioController.Play("playerhit");
            levelManager.SuicidePlayer(tag);
        }
    }

    private void FireBloodParticles()
    {
        ParticleSystem particles = (ParticleSystem) Instantiate(blood, transform.position, Quaternion.identity);
        particles.Play();
        Destroy(gameObject);
    }
}