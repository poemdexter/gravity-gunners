﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColliderLoader : MonoBehaviour 
{
    public TextAsset levelData;
    public GameObject colliderPrefab;

	void Start () 
    {
        if (!levelData) Debug.Log("missing levelData JSON file.");

        Dictionary<string, object> hash = levelData.text.dictionaryFromJson();
        List<object> layersList = (List<object>)hash["layers"];
        for (int i = 0; i < layersList.Count; i++)
        {
            Dictionary<string, object> layerHash = (Dictionary<string, object>)layersList[i];
            if (layerHash["name"].ToString().Equals("Colliders"))
            {
                List<object> colliders = (List<object>)layerHash["objects"];
                for (int j = 0; j < colliders.Count; j++)
                {
                    Dictionary<string, object> collider = (Dictionary<string, object>)colliders[j];
                    int x = int.Parse(collider["x"].ToString()) / 20;
                    int y = int.Parse(collider["y"].ToString()) / 20;
                    int width = int.Parse(collider["width"].ToString()) / 20;
                    int height = int.Parse(collider["height"].ToString()) / 20;
                    CreateCollider(x, y, width, height);
                }
            }
        }
	}

    private void CreateCollider(int x, int y, int width, int height)
    {
        float xPos = (16 * -1.25f) + (x * 1.25f) + ((width / 2) * 1.25f) - (((width-1) % 2) * .625f);
        float yPos = (10 * 1.25f) - (y * 1.25f) - ((height / 2) * 1.25f) + (((height-1) % 2) * .625f);
        GameObject collider = (GameObject)Instantiate(colliderPrefab, new Vector3(xPos, yPos, 0f), Quaternion.identity);
        collider.transform.parent = this.transform;
        BoxCollider2D box = collider.GetComponent<BoxCollider2D>();
        box.size = new Vector2(1.25f * width, 1.25f * height);
    }
}
