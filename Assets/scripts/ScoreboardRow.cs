﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngineInternal;

public enum PlayerTag
{
    P1,
    P2,
    P3,
    P4
}

public class ScoreboardRow : MonoBehaviour
{
    public SpriteRenderer icon;
    public SpriteRenderer portrait;
    public PlayerTag playerTag;
    public SpriteRenderer[] markers;

    public bool DoneShowingNewKills { get; private set; }
    public bool DoneShowingNewSuicides { get; private set; }

    private GlobalController global;
    private GlobalController GetGlobal()
    {
        if (global == null)
        {
            global = GameObject.FindGameObjectWithTag("GlobalController").GetComponent<GlobalController>();
        }
        return global;
    }

    private void Start()
    {
        // set proper icon
        Sprite s = GetGlobal().GetPlayerPortrait(playerTag.ToString());
        if (s != null)
        {
            icon.sprite = GetGlobal().GetPlayerPortraitBG(playerTag);
            portrait.sprite = s;

            // set proper amount of dots
            int winsNeeded = GetGlobal().GetGameWinAmount();
            for (int j = 0; j < 10; j++)
            {
                if (j >= winsNeeded)
                {
                    markers[j].enabled = false;
                }
            }

            // set previous score
            GlobalController.Player player = global.GetPlayer(playerTag.ToString());
            if (player != null)
            {
                for (int i = 0; i < player.Score; i++)
                {
                    markers[i].sprite = global.whiteSkull;
                }
            }
        }
        else // not playing
        {
            for (int j = 0; j < 10; j++)
            {
                 markers[j].enabled = false;
            }
        }
    }

    // show new kills with small delay between.  if no new kills, just flag us as done
    public void ShowNewKills()
    {
        GlobalController.Player player = GetGlobal().GetPlayer(playerTag.ToString());
        if (player != null)
        {
            if (player.Kills > 0)
            {
                StartCoroutine(ShowKillOneAtTime(player));
            }
            else
            {
                DoneShowingNewKills = true;
            }
        }
        else
        {
            DoneShowingNewKills = true;
        }
    }

    // show new suicides with small delay between.  if no new suicides, just flag us as done
    public void ShowNewSuicides()
    {
        GlobalController.Player player = GetGlobal().GetPlayer(playerTag.ToString());
        if (player != null)
        {
            if (player.Suicides > 0 && player.Score > 0)
            {
                StartCoroutine(ShowSuicideOneAtTime(player));
            }
            else
            {
                DoneShowingNewSuicides = true;
                player.Suicides = 0;
            }
        }
        else
        {
            DoneShowingNewSuicides = true;
        }
    }

    private IEnumerator ShowKillOneAtTime(GlobalController.Player player)
    {
        for (int i = player.Score; i < player.Score + player.Kills; i++)
        {
            AudioController.Play("gainskull");
            markers[i].sprite = global.goldSkull;
            yield return new WaitForSeconds(.5f);
        }
        player.Score += player.Kills;
        player.Kills = 0;
        DoneShowingNewKills = true;
    }

    private IEnumerator ShowSuicideOneAtTime(GlobalController.Player player)
    {
        AudioController.Play("loseskull");
        markers[player.Score - 1].sprite = global.redSkull;
        yield return new WaitForSeconds(.5f);
        player.Score -= player.Suicides;
        player.Suicides = 0;
        DoneShowingNewSuicides = true;
    }
}