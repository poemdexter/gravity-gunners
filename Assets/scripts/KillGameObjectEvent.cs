﻿using UnityEngine;
using System.Collections;

public class KillGameObjectEvent : MonoBehaviour 
{	
	public void KillGameObject() 
    {
        Destroy(this.gameObject);
	}
}
