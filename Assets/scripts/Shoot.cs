﻿using System.Runtime.Serialization.Formatters;
using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{
    public GameObject bullet;
    public Vector2 fireOffset;
    public float kickback;
    public int maxBullets;
    public GameObject fireEffect;
    private int bulletCount;
    public bool CanShoot { get; set; }
    private GlobalController global;
    
    public void Start()
    {
        CanShoot = true;
    }

    private GlobalController GetGlobalController()
    {
        if (global == null) global = GameObject.FindGameObjectWithTag("GlobalController").GetComponent<GlobalController>();
        return global;
    }

    public void BulletDestroyed()
    {
        bulletCount--;
    }

    private void Update()
    {
        if (CompareTag("P1") && Input.GetButtonDown("A_1"))
        {
            Fire();
        }
        if (CompareTag("P2") && Input.GetButtonDown("A_2"))
        {
            Fire();
        }
        if (CompareTag("P3") && Input.GetButtonDown("A_3"))
        {
            Fire();
        }
        if (CompareTag("P4") && Input.GetButtonDown("A_4"))
        {
            Fire();
        }
    }

    private void Fire()
    {
        if (CanShoot && bulletCount < maxBullets)
        {
            AudioController.Play("shootgun");
            fireEffect.GetComponent<Animator>().SetTrigger("PlayWeaponEffect");
            Vector3 right = (transform.right*fireOffset.x);
            Vector3 up = (transform.up*fireOffset.y);
            var b = (GameObject) Instantiate(bullet, transform.position + right + up, transform.rotation);
            GetComponent<Rigidbody2D>().AddForce(-transform.right*kickback);
            b.GetComponent<Owner>().Owns = tag;
            bulletCount++;
        }
    }
}