﻿using UnityEngine;
using System.Collections;

public class TitleController : MonoBehaviour
{
    public GameObject pressToStart;
    public SpriteRenderer fadeOut;
    private bool isSelected;
    private double currentBlinkTime, currentFadeTime;
    public double blinkTime, fadeTime;

    private void Start()
    {
        var global = GameObject.FindGameObjectWithTag("GlobalController");
        if (global != null)
        {
            Destroy(global);
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("A_1") || Input.GetButtonDown("A_2") || Input.GetButtonDown("A_3") || Input.GetButtonDown("A_4"))
        {
            if (!isSelected)
            {
                AudioController.Play("selectedthing");
                isSelected = true;
            }
        }

        if (isSelected)
        {
            if ((currentBlinkTime += Time.deltaTime) > blinkTime)
            {
                currentBlinkTime = 0;
                pressToStart.SetActive(!pressToStart.activeSelf);
            }
            if ((currentFadeTime += Time.deltaTime) < fadeTime)
            {
                fadeOut.color = new Color(1f,1f,1f,(float)(currentFadeTime / fadeTime));
            }
            else
            {
                Application.LoadLevel("choose");
            }
        }
    }
}