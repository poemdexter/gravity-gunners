﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using UnityEngine;
using System.Collections;

public class GlobalController : MonoBehaviour
{
    public class Player
    {
        public int Number { get; set; }
        public String Tag { get; set; }
        public Sprite Suit { get; set; }
        public Sprite Face { get; set; }
        public Sprite Portrait { get; set; }
        public GameObject Bullet { get; set; }
        public String Color { get; set; }
        public int Score { get; set; }
        public int Kills { get; set; }
        public int Suicides { get; set; }
        public bool Alive { get; set; }
    }

    // for scoreboard
    public Sprite whiteSkull, goldSkull, redSkull;
    public Sprite[] portraits;
    public Sprite yvPortrait, meaniePortrait;
    public Sprite[] portraitBGs;
    public GameObject[] bullets;
    public String[] levels;
    private int gameWinAmount;

    private int currentLevel = 0;
    private String gameWinnerTag;
    private List<Player> playerList = new List<Player>();


    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public int GetGameWinAmount()
    {
        return gameWinAmount;
    }

    public static String GetColorBySelection(int selection)
    {
        switch (selection)
        {
            case 0:
                return "blue";
            case 1:
                return "purple";
            case 2:
                return "orange";
            case 3:
                return "pink";
            case 4:
                return "yellow";
            case 5:
                return "violet";
            case 6:
                return "green";
            case 7:
                return "red";
            default:
                return "yellow";
        }
    }

    public void Setup()
    {
        // determine how many wins are needed to finish game
        switch (playerList.Count)
        {
            case 2:
                gameWinAmount = 5;
                break;
            case 3:
                gameWinAmount = 7;
                break;
            case 4:
                gameWinAmount = 10;
                break;
        }
    }

    public String GetGameWinnerTag()
    {
        return gameWinnerTag;
    }

    public void HaltAllPlayers()
    {
        foreach (Player p in playerList)
        {
            GameObject player = GameObject.FindGameObjectWithTag(p.Tag);
            if (player != null)
            {
                player.GetComponent<Jetpack>().CanSpin = false;
                player.GetComponent<Jetpack>().CanBoost = false;
                player.GetComponent<Shoot>().CanShoot = false;
                player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }
        }
    }

    public String GetNextLevelName()
    {
        currentLevel = (currentLevel + 1 >= levels.Length) ? 0 : currentLevel + 1;
        return levels[currentLevel];
    }

    public void AddPlayer(int number, String tag, PlayerInfo playerInfo)
    {
        playerList.Add(new Player {
            Tag = tag,
            Suit = playerInfo.suit,
            Face = playerInfo.face,
            Number = number,
            Portrait = GetPortrait(playerInfo),
            Bullet = bullets[number - 1],
            Color = GetColorBySelection(playerInfo.detailChoice),
            Alive = true 
        });
    }

    private Sprite GetPortrait(PlayerInfo playerInfo)
    {
        if (playerInfo.name.Equals("Meanie")) { return meaniePortrait; }
        else if (playerInfo.name.Equals("Y.V.")) { return yvPortrait; }
        else { return portraits[playerInfo.detailChoice]; }
    }

    public List<Player> GetPlayerList()
    {
        return playerList;
    }

    public Player GetPlayer(String playerTag)
    {
        return playerList.FirstOrDefault(x => x.Tag == playerTag);
    }

    /// <summary>
    /// Adds to player's Kills count.
    /// Returns true if max number of score needed to end game has been acheived.
    /// </summary>
    /// <param name="playerTag"></param>
    /// <returns></returns>
    public bool AddToKills(String playerTag)
    {
        Player player = playerList.First(x => x.Tag == playerTag);
        player.Kills++;
        if (player.Kills + player.Score >= gameWinAmount)
        {
            gameWinnerTag = player.Tag;
            return true;
        }
        return false;
    }

    public void AddToSuicides(String playerTag)
    {
        playerList.First(x => x.Tag == playerTag).Suicides++;
    }

    public void KillPlayer(String playerTag)
    {
        playerList.First(x => x.Tag == playerTag).Alive = false;
    }

    public String GetWinnerTag()
    {
        return playerList.First(x => x.Alive).Tag;
    }

    public Sprite GetPlayerPortrait(String playerTag)
    {
        Player p = playerList.FirstOrDefault(x => x.Tag == playerTag);
        return p != null ? p.Portrait : null;
    }

    public Sprite GetPlayerPortraitBG(PlayerTag playerTag)
    {
        switch (playerTag)
        {
            case PlayerTag.P1: return portraitBGs[0];
            case PlayerTag.P2: return portraitBGs[1];
            case PlayerTag.P3: return portraitBGs[2];
            case PlayerTag.P4: return portraitBGs[3];
            default: return null;
        }
    }

    public void MakePlayersAliveForNextRound()
    {
        foreach (Player p in playerList)
        {
            p.Alive = true;
        }
    }
}