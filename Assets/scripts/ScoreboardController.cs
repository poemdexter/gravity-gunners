﻿using UnityEngine;
using System.Collections;

public class ScoreboardController : MonoBehaviour
{
    public ScoreboardRow row1, row2, row3, row4;
    private bool showKills1, showKills2, showKills3, showKills4;
    private bool shownK1, shownK2, shownK3, shownK4;
    private bool showSuicides1, showSuicides2, showSuicides3, showSuicides4;
    private bool shownS1, shownS2, shownS3, shownS4;

    private bool sbDone; // for starting Coroutine to wait 3s to view scores
    public bool ScoreboardDone { get; private set; } // for flagging us ready to load new level

    public void StartScoreboard()
    {
        // show scoreboard to players
        gameObject.SetActive(true);

        // show new kills to players after small delay
        StartCoroutine(DelayBeforeUpdateScores());
    }

    private IEnumerator DelayBeforeUpdateScores()
    {
        yield return new WaitForSeconds(1f);
        showKills1 = true;
    }

    private void StartDoneDelay()
    {
        StartCoroutine(DelayBeforeScoreboardDone());
    }

    private IEnumerator DelayBeforeScoreboardDone()
    {
        yield return new WaitForSeconds(3f);
        ScoreboardDone = true;
    }

    private void Update()
    {
        // show the new kills
        if (!shownK1 && showKills1)
        {
            row1.ShowNewKills();
            shownK1 = true;
        }
        if (!shownK2 && showKills2)
        {
            row2.ShowNewKills();
            shownK2 = true;
        }
        if (!shownK3 && showKills3)
        {
            row3.ShowNewKills();
            shownK3 = true;
        }
        if (!shownK4 && showKills4)
        {
            row4.ShowNewKills();
            shownK4 = true;
        }

        // make sure previous row is done before showing new row kills
        if (row1.DoneShowingNewKills)
        {
            showKills2 = true;
        }
        if (row2.DoneShowingNewKills)
        {
            showKills3 = true;
        }
        if (row3.DoneShowingNewKills)
        {
            showKills4 = true;
        }
        if (row4.DoneShowingNewKills)
        {
            showSuicides1 = true;
        }

        // show the new suicides
        if (!shownS1 && showSuicides1)
        {
            row1.ShowNewSuicides();
            shownS1 = true;
        }
        if (!shownS2 && showSuicides2)
        {
            row2.ShowNewSuicides();
            shownS2 = true;
        }
        if (!shownS3 && showSuicides3)
        {
            row3.ShowNewSuicides();
            shownS3 = true;
        }
        if (!shownS4 && showSuicides4)
        {
            row4.ShowNewSuicides();
            shownS4 = true;
        }

        // make sure previous row is done before showing new row suicides
        if (row1.DoneShowingNewSuicides)
        {
            showSuicides2 = true;
        }
        if (row2.DoneShowingNewSuicides)
        {
            showSuicides3 = true;
        }
        if (row3.DoneShowingNewSuicides)
        {
            showSuicides4 = true;
        }
        if (row4.DoneShowingNewSuicides && !sbDone)
        {
            // we're done with scoreboard, wait a delay and flag us ready to move on to new level
            sbDone = true;
            StartDoneDelay();
        }
    }
}