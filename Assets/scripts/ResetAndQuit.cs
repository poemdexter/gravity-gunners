﻿using UnityEngine;
using System.Collections;

public class ResetAndQuit : MonoBehaviour
{
    // reset game fields
    private float b1Time, b2Time, b3Time, b4Time;
    private bool b1Down, b2Down, b3Down, b4Down;

    private void Update()
    {
        // tapping back will reset the game
        // tapping reset or holding back will exit the game
        if (Input.GetButtonDown("Back_1"))
        {
            b1Down = true;
        }
        if (Input.GetButtonDown("Back_2"))
        {
            b2Down = true;
        }
        if (Input.GetButtonDown("Back_3"))
        {
            b3Down = true;
        }
        if (Input.GetButtonDown("Back_4"))
        {
            b4Down = true;
        }
        if (Input.GetButtonDown("Reset"))
        {
            Application.Quit();
        }

        if (Input.GetButtonUp("Back_1"))
        {
            Application.LoadLevel("logo");
        }
        if (Input.GetButtonUp("Back_2"))
        {
            Application.LoadLevel("logo");
        }
        if (Input.GetButtonUp("Back_3"))
        {
            Application.LoadLevel("logo");
        }
        if (Input.GetButtonUp("Back_4"))
        {
            Application.LoadLevel("logo");
        }
        if (Input.GetButtonUp("Reset"))
        {
            Application.LoadLevel("logo");
        }

        if (b1Down && (b1Time += Time.deltaTime) > 3f)
        {
            Application.Quit();
        }
        if (b2Down && (b2Time += Time.deltaTime) > 3f)
        {
            Application.Quit();
        }
        if (b3Down && (b3Time += Time.deltaTime) > 3f)
        {
            Application.Quit();
        }
        if (b4Down && (b4Time += Time.deltaTime) > 3f)
        {
            Application.Quit();
        }
    }
}